Name
====

lua-resty-shell - 基于lua-nginx-module的cosocket API，实现非阻塞的执行系统shell命令。

Description
===========

依赖第三方[**sockproc**](https://github.com/juce/sockproc)。


Synopsis
========

首先运行sockproc，以监听一个UNIx socket：

```
    $ ./sockproc /tmp/shell.sock
```

nginx配置示例:

```
    lua_package_path "/path/to/lua-resty-shell/lib/?.lua;;";

    server {
        location /test {
            content_by_lua_block {
                local shell = require 'resty.shell'

                local sl = shell.new()
                if not sl then
                    return
                end

				local sock = 'unix:/tmp/shell.sock'    -- sockproc监听的socket
				
                local res, err = sl:connect(sock)
                if not res then
                    return
                end

                local res, err = sl:send_command({
				    cmd = "uname -a",
				})
                if not res then
                    return
                end

                local status, out, err = sl:read_response()

                sl:close()
            }
        }
    }
```

Methods
=======

new
---
`syntax: sl, err = shell.new()`

创建一个shell对象。创建失败时，返回nil，并返回错误信息。

connect
-------
`syntax: ok, err = sl:connect("unix:/path/to/unix.sock", options_table?)`

连接UNIX socket。


set_timeout
-----------
`syntax: sl:set_timeout(time)`

设置超时时间（毫秒级）。

set_keepalive
-------------
`syntax: ok, err = sl:set_keepalive(max_idle_timeout, pool_size)`

将当期连接放入连接池中。

成功时，将返回1。失败时，将会返回nil和错误信息。

close
-----
`syntax: ok, err = sl:close()`

关闭当前连接。

成功时，将返回1。失败时，将会返回nil和错误信息。

send_command
------------
`syntax: ok, err = sl:send_command(params)`

发送要执行的系统命令。

* `params`

它是一个lua table。通过内部*cmd*来指定需要执行的命令语句。

read_response
-------------
`syntax: status, out, err = sl:read_response()`

如果用需要，可以用来读取系统命令执行后的响应内容。

See Also
========
* the ngx_lua module: https://github.com/openresty/lua-nginx-module/#readme
* the Sockproc daemon: https://github.com/juce/sockproc
